/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package estudiante;

/**
 *
 * @author Administrador
 */
public class Estudiante {
 //atributo
    public String nombre;
    public String apellido;
    public String direccion;
    public String lugarnacimiento;
    public int edad;
    //metodos
    public void setNombre(String x){
        
        nombre=x; 
    }
    public void setApellido(String x){
        apellido=x;
    }
    public void setDireccion(String x){
        direccion=x;
    }
    public void setLugarnacimiento(String x){
        lugarnacimiento=x;
    }
    public void setEdad(int x){
        edad=x;
    }
    public String getNombre(){
        return nombre;
    }
    public String getApellido(){
        return apellido;
    }
    public String getDireccion(){
        return direccion;
    }
    public String getLugarnacimiento(){
        return lugarnacimiento;
    }
    public double getEdad(){
        return edad;
    }
}
